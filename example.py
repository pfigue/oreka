#coding: utf-8


from oreka import VirtualEnv as VE


def main():
	ve = VE()

	# Create a new virtualenv in /tmp/oreka_example/, using /usr/bin/python as Python Interpreter
	result = ve.create_virtualenv('/usr/bin/python2.7', '/tmp/oreka_example/')

	# Install Flask inside
	result = ve.install_requirements(requirements_list=['flask',])

	# Report the path to the user
	print('The virtualenv was properly installed into %s.' % ve.get_directory())

	# Removal of the virtualenv has to be manual, like `rm -rf /tmp/oreka_example/`
	# But you can program a .remove_virtualenv() method, if you want.


main()
