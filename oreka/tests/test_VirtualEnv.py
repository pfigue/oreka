from unittest import TestCase
from nose.tools import (assert_equal, nottest, raises)

from oreka import VirtualEnv

class VirtualEnvTests(TestCase):
    @staticmethod
    def mock_run(cmd):
        return cmd

    def setUp(self):
        self.venv = VirtualEnv(runcmd_cb=self.mock_run)
        self.venv.virtualenv_path = '/usr/bin/virtualenv'

    def test_create_virtualenv(self):
        result = self.venv.create_virtualenv('python2.7','/tmp/test/')
        assert_equal(result,
                    '/usr/bin/virtualenv --python=python2.7 /tmp/test/')

    def test_install_requirements_file(self):
        self.venv.create_virtualenv('python2.7','/tmp/test/')
        result = self.venv.install_requirements(requirements_filename='requirements.pip')
        assert_equal(result,
                     '/tmp/test/bin/pip install --requirement=requirements.pip')

    @raises(Exception)
    def test_install_requirements_wo_requirements(self):
        self.venv.create_virtualenv('python2.7','/tmp/test/')
        result = self.venv.install_requirements()

    @raises(Exception)
    def test_install_requirements_w_2_requirements(self):
        self.venv.create_virtualenv('python2.7','/tmp/test/')
        result = self.venv.install_requirements(requirements_filename='abc', requirements_list=['abc==1.0',])

    def test_install_requirements_list(self):
        self.venv.create_virtualenv('python2.7','/tmp/test/')
        sammlung = [
            'distribute==0.6.28',
            'south==0.8.2',
        ]
        result = self.venv.install_requirements(requirements_list=sammlung)
        assert_equal(result,
                     '/tmp/test/bin/pip install distribute==0.6.28 south==0.8.2')
