from os.path import join
from tempfile import mkdtemp
from subprocess import check_call


class VirtualEnv(object):
    virtualenv_path = '/usr/bin/virtualenv'

    def install_requirements(self, requirements_filename=None, requirements_list=None):
        """
        FIXME: an update option?
        """
        if not (requirements_filename or requirements_list):
            raise Exception('You must specify requirements!')
        if requirements_filename and requirements_list:
            raise Exception('You must specify either a requirements file or a requirements list!')

        pip_dir = join(self.directory, 'bin/pip')

        if requirements_filename:
            return self.run(
                '%(pip)s install --requirement=%(requirements)s' % {
                'pip': pip_dir,
                'requirements': requirements_filename,
            })
        else:
            text = ' '.join(requirements_list)
            return self.run(
                '%(pip)s install %(requirements)s' % {
                'pip': pip_dir,
                'requirements': text,
            })


    def create_virtualenv(self, python_interpreter_path, directory=None,):
        self.python_interpreter_path = python_interpreter_path
        self.directory = directory if directory else mkdtemp()

        return self.run(
            '%(virtualenv)s --python=%(python)s %(target)s' % {
            'virtualenv': self.virtualenv_path,
            'python': self.python_interpreter_path,
            'target': self.directory,
        })

    def get_directory(self):
        return self.directory if self.directory else None

    @staticmethod
    def _run(cmd):
        check_call(cmd, shell=True)

    def __init__(self, runcmd_cb=None):
        self.run = runcmd_cb if runcmd_cb else self._run
