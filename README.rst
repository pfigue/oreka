What is Oreka?
==============

Oreka is a python module to create Python Virtual Environments from Python scripts.

How to install Oreka?
=====================

You may want to run this from inside a virtual environment, or with root privileges to install globally to all your system:

::

	pip install oreka

Check it with:

::

	python -c "from oreka import VirtualEnv"

It shouldn't raise any error.


How to run the example.py?
==========================

In the example you can see how to create a virtualenv and install requirements on it.

First, let's create and activate a testing virtualenv:

::

	mkvirtualenv -p /usr/bin/python2.7 /tmp/testing_oreka/
	source /tmp/testing_oreka/bin/activate
	(testing_oreka) $

Prepare the pypi package and install it into the venv:

::

	(testing_oreka) $ python setup.py install

The same virtualenv should work to prepare the package and to test it.

Check it:

::

	(testing_oreka) $ pip freeze
	oreka==0.1
	wsgiref==0.1.2


And run the example:

::

	(testing_oreka) $ python example.py

You should get no error.

The source code at the moment is quite small, so reading it or the tests should not be complicated and should provide some help on how to use the system.
