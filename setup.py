from setuptools import setup


with open('README.rst', 'r') as f:
  long_description = f.read()


setup(name='oreka',
      version='0.1',
      description='Virtualenv management from python',
      long_description=long_description,
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'Topic :: System :: Software Distribution',
      ],
      keywords='virtualenv virtual environment scripting',
      url='https://bitbucket.org/pfigue/oreka',
      author='Pablo Figue',
      author_email='pablo.gfigue@gmail.com',
      license='MIT',
      packages=['oreka'],
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'],
)
